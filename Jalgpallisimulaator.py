import pygame
import time
import random
import math
import copy

pygame.init()

#Display dimensions
display_width = 800
display_height = 600

#Colors
black = (0,0,0)
white = (255,255,255)
red = (255,0,0)
green = (0,255,0)
blue = (0,0,255)

#Set display and heading
gameDisplay = pygame.display.set_mode((display_width,display_height))
pygame.display.set_caption('Jalgpallisimulaator')

# set cloc
clock = pygame.time.Clock()

# robot image and dimentions
robot_width = 50

# goal dimentsions
goal_width = 5
goal_height = 60

# ball dimentions and speed when kicked
ball_width = 9
ball_speed = 5


# displays robot image
def robot(image, x,y):
    gameDisplay.blit(image,(x,y))

# creates images
def balls(thingx, thingy, thingw, thingh, color):
    pygame.draw.rect(gameDisplay, color, [thingx, thingy, thingw, thingh])

# rotates robot
def rot_center(image, angle):
    """rotate an image while keeping its center and size"""
    orig_rect = image.get_rect()
    rot_image = pygame.transform.rotate(image, angle)
    rot_rect = orig_rect.copy()
    rot_rect.center = rot_image.get_rect().center
    rot_image = rot_image.subsurface(rot_rect).copy()
    return rot_image

def ballvalues(ball_cordinates, x, y, ball_width, robot_width):

    #list for calculationg ball values
    ball_values = []

    # calculates ball values only by distance
    for i in range(len(ball_cordinates)):
        ball_value = math.sqrt((ball_cordinates[i][0] + ball_width / 2 - (x+robot_width))**2 + (ball_cordinates[i][1] + ball_width / 2 - (y + robot_width/2))**2)
        ball_values.append(ball_value)

     # finds best ball by value and it's index in ball_values list
        best_value = min(ball_values)
        indeks = ball_values.index(best_value)

    return ball_values, indeks

def gofortheball(ball_cordinates, x, y, robot_width, ball_width, indeks):
    # how to find the ball if robot behind the ball
    # if robot behind the ball
    min_distance_from_ball = 2
    if x + robot_width < ball_cordinates[indeks][0]:
        # if robot further on x axes than y axes
        if  ball_cordinates[indeks][0] - (x + robot_width) > abs(ball_cordinates[indeks][1] + ball_width / 2 - (y + robot_width / 2)):
            x  += 2
        # if robot lower than ball
        elif ball_cordinates[indeks][1] + ball_width / 2 > (y + robot_width / 2):
            y += 2
        # if robot higher than ball
        else:
            y -= 2
    # if robot in front of the ball
    else:
        # if robot not on the same y axes as the ball
        if ball_cordinates[indeks][1] - min_distance_from_ball > y + robot_width or ball_cordinates[indeks][1] + ball_width + min_distance_from_ball < y:
            x -= 2
        # if robot on the same line with the ball
        else:
            # if ball center lower than robot center
            if ball_cordinates[indeks][1] + ball_width / 2 > y + robot_width / 2:
                y -= 2
            # if ball center higher than robot center
            else:
                y += 2

    return x, y

# checks if robot has the ball if diference between robot and ball on both axes is smaller than 2
def ifrobothastheball(ball_cordinates, x, y, robot_width, ball_width, indeks):
    if abs(x + robot_width - (ball_cordinates[indeks][0])) < 2 and abs(ball_cordinates[indeks][1] + ball_width / 2 - (y + robot_width / 2)) < 2:
        return True
    else:
        return False

# draws balls
def drawtheballs(ball_cordinates,ball_width, red):
    for i in range(len(ball_cordinates)):
        balls(ball_cordinates[i][0], ball_cordinates[i][1], ball_width, ball_width, red)

# create ball corindates
def createballcordinates(n, display_width, ball_width):
    ball_cordinates = []
    for i in range(n):
        thing_startx = random.randrange(0, display_width - ball_width)
        thing_starty = random.randrange(0, display_height - ball_width)
        ball_cordinates.append([thing_startx, thing_starty, False, 0, 0, 0])
    ball_cordinates.append([450,294, False,0,0,0])
    ball_cordinates.append([575,294, False,0,0,0])
    return ball_cordinates

# calculates angel to the goal
def calcangle(x, y, display_width, display_height):
    x_distance_from_goal = display_width - (x + robot_width)
    y_distance_from_goal = display_height/2 - (y + robot_width / 2)
    angle = round(math.atan(y_distance_from_goal / x_distance_from_goal) * 57.3, 0)
    return angle


def changeballcordinates1(ball_cordinates, display_width, count):

    for i in range(len(ball_cordinates)):
            ball_cordinates[i][0] += ball_cordinates[i][3]
            ball_cordinates[i][1] += ball_cordinates[i][4]

    if count > 0:
        count -= 1

    for i in ball_cordinates[:]:
        if i[0] + i[3] > display_width:
            ball_cordinates.remove(i)

    return ball_cordinates, count

def game_loop():

    robotImg = pygame.image.load('bee.jpg')

    # robot starting point
    x = ((display_width - robot_width)  * 0.5)
    y = ((display_height - robot_width) * 0.5)

    # creates n x, y pairs - ball cordinates
    ball_cordinates = createballcordinates(11, display_width, ball_width)

    # sets beginning parameters
    game_exit = False
    robot_has_the_ball = False

    dif_angle = 0
    count = 0

    while not game_exit:

        # how to quit the game
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                quit()

        # fills game display
        gameDisplay.fill(green)

        # draws borders, middle line and goals
        balls(0,0,display_width,5,white) # upper border
        balls(0,display_height - 5,display_width,5,white) # lower border
        balls(0,0,5,display_height,white) # left border
        balls(display_width - 5,0,5,display_height, white) #right border
        balls((display_width-5)/2,0,5,display_height,white) # middle line
        balls(0, (display_height-goal_height)/2,goal_width,goal_height,black) # left goal
        balls(display_width-goal_width, (display_height-goal_height)/2,goal_width,goal_height,black) # right goal

        # if there are still balls left
        if len(ball_cordinates) > 0:

            #draws the balls
            drawtheballs(ball_cordinates,ball_width,red)

            #calculates ball values and finds best value as index
            ball_values, index = ballvalues(ball_cordinates, x, y, ball_width, robot_width)

            # checks if robot has the ball
            robot_has_the_ball = ifrobothastheball(ball_cordinates, x, y, robot_width, ball_width, index)

            # checks if ball has not been shot just a while ago
            if count == 0:
                # if robot dosn't have the ball
                if robot_has_the_ball == False:

                    #calculates new x and y cordinates for the robot
                    if count == 0:
                        x, y = gofortheball(ball_cordinates, x, y, robot_width, ball_width, index)
                        if y + robot_width / 2 > display_height / 2:
                            where_to_go = 'UP'
                        else:
                            where_to_go = 'DOWN'

                # if robot has the ball
                else:
                    # calculates angle to the goal
                    angle = calcangle(x, y, display_width, display_height)


                    # if angle is to small move the robot with the ball
                    if abs(angle) < 70:
                        # reduces angle
                        dif = 2
                        if angle + dif_angle >= dif:
                            robotImg = rot_center(robotImg, -dif)
                            dif_angle -= dif
                        elif angle + dif_angle <= -dif:
                            robotImg = rot_center(robotImg, dif)
                            dif_angle += dif

                        #shoots the ball
                        if abs(angle + dif_angle) < dif:

                            ball_on_the_way = False
                            shot_ball_coordinates = []
                            # makes list of ball cordinates
                            for sb_x in range(ball_cordinates[index][0],display_width):
                                for sb_y in range(ball_cordinates[index][1], ball_cordinates[index][1]+ball_width):
                                    shot_ball_coordinates.append([sb_x,int(sb_y + (sb_x-ball_cordinates[index][0]) * math.tan(angle/53.7))])

                            # checks if ball corners are in shot ball cordinates list
                            for ball_id in range(len(ball_cordinates)):
                                ball_on_the_way = False
                                if ball_id != index:
                                    if ([ball_cordinates[ball_id][0], ball_cordinates[ball_id][1]] in shot_ball_coordinates or
                                        [ball_cordinates[ball_id][0] + 9, ball_cordinates[ball_id][1]] in shot_ball_coordinates or
                                        [ball_cordinates[ball_id][0], ball_cordinates[ball_id][1] + 9] in shot_ball_coordinates or
                                        [ball_cordinates[ball_id][0] + 9, ball_cordinates[ball_id][1] + 9] in shot_ball_coordinates):
                                        ball_on_the_way = True

                            if ball_on_the_way == True:
                                if where_to_go == 'DOWN':
                                    y += 2
                                    ball_cordinates[index][1] += 2
                                else:
                                    y -= 2
                                    ball_cordinates[index][1] -= 2

                            else:
                                ball_cordinates[index][2] = True
                                ball_cordinates[index][3] = math.cos(dif_angle / 53.7) * ball_speed
                                ball_cordinates[index][4] = math.sin(-dif_angle / 53.7) * ball_speed
                                robot_has_the_ball = False
                                count = 40
                                dif_angle = 0

                    else:
                        x -= 1
                        ball_cordinates[index][0] -= 1
                        if y < display_height / 2:
                            y += 1
                            ball_cordinates[index][1] += 1
                        else:
                            y -= 1
                            ball_cordinates[index][1] -= 1

            #change ball cordinates
            ball_cordinates, count = changeballcordinates1(ball_cordinates, display_width, count)

            robot(robotImg, x, y)
            pygame.display.update()
            clock.tick(60)

            if len(ball_cordinates) == 0:
                game_exit = True

game_loop()
pygame.quit()
quit()